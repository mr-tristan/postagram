import java.lang.reflect.Array;
import java.util.ArrayList;

public class Account {
    private String username;
    private String password;
    private ArrayList<Post> posts;

    public Account(String username, String password) {
        this.username = username;
        this.password = password;
        this.posts = new ArrayList<Post>();
    }

    public String toString() {
        return this.getUsername();
    }

    public void addPost(Post post) {
        this.posts.add(post);
    }

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public ArrayList<Post> getPosts() {
        return this.posts;
    }
}
