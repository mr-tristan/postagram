import java.util.ArrayList;

public class Postagram {
    private Account currentUser;
    private ArrayList<Account> userAccounts;

    public enum TimelineSort {
        DATE,
        LIKES,
        NONE
    }

    public Postagram() {
        this.userAccounts = new ArrayList<Account>();
    }

    public boolean post(Account acc, Post post) {
        for (Account a : this.userAccounts) {
            if (a.getUsername().equals(acc.getUsername()) &&
                a.getPassword().equals(acc.getPassword())) {
                a.addPost(post);
                return true;
            }
        }

        return false;
    }

    public boolean likePost(String username, Account liker, int postNumber) {
        for (Account a : this.userAccounts) {
            if (a.getUsername().equalsIgnoreCase(username)) {
                try {
                    return a.getPosts().get(postNumber - 1).addLike(liker);
                } catch (ArrayIndexOutOfBoundsException e) {
                    return false;
                }
            }
        }
        return false;
    }

    public boolean registerAccount(String username, String password) {
        Account acc = new Account(username, password);
        for (Account a : this.userAccounts) {
            if (a.getUsername().equalsIgnoreCase(acc.getUsername())) {
                return false;
            }
        }

        this.userAccounts.add(acc);
        return true;
    }

    public boolean isCurrentUserSignedIn() {
        if (this.currentUser == null) {
            return false;
        }
        return true;
    }

    public Account getCurrentUser() {
        return this.currentUser;
    }

    public boolean signIn(Account acc) {
        for (Account a : this.userAccounts) {
            // THIS IS TERRIBLE SECURITY!
            if (a.getUsername().equalsIgnoreCase(acc.getUsername()) &&
                a.getPassword().equals(acc.getPassword())) {
                this.currentUser = a;
                return true;
            }
        }

        return false;
    }

    public ArrayList<Account> getUserAccounts() {
        return this.userAccounts;
    }

    public ArrayList<Account> sortAccountPostsByDate() {
        ArrayList<Account> sortedUserAccounts = this.userAccounts;

        for (Account acc : sortedUserAccounts) {
            ArrayList<Post> userPosts = acc.getPosts();

            // Bubble sort!
            for (int i = 0; i < userPosts.size(); i++) {
                for (int j = 1; j < userPosts.size(); j += 2) {
                    if (userPosts.get(j).getDate().compareTo(userPosts.get(j - 1).getDate()) > 0) {
                        Post tmp = userPosts.get(j);
                        userPosts.set(j, userPosts.get(j - 1));
                        userPosts.set(j - 1, tmp);
                    }
                }
            }

            acc.setPosts(userPosts);
        }

        return sortedUserAccounts;
    }

    public ArrayList<Account> sortAccountPostsByLikes() {
        ArrayList<Account> sortedUserAccounts = this.userAccounts;

        for (Account acc : sortedUserAccounts) {
            ArrayList<Post> userPosts = acc.getPosts();

            // Bubble sort!
            for (int i = 0; i < userPosts.size(); i++) {
                for (int j = 1; j < userPosts.size(); j += 2) {
                    if (userPosts.get(j).getLikes() > userPosts.get(j - 1).getLikes()) {
                        Post tmp = userPosts.get(j);
                        userPosts.set(j, userPosts.get(j - 1));
                        userPosts.set(j - 1, tmp);
                    }
                }
            }

            acc.setPosts(userPosts);
        }

        return sortedUserAccounts;
    }

    public void displayTimeline(TimelineSort sort) {
        ArrayList<Account> sortedUserAccounts = this.userAccounts;
        switch (sort) {
            case DATE:
                sortedUserAccounts = sortAccountPostsByDate();
                break;
            case LIKES:
                sortedUserAccounts = sortAccountPostsByLikes();
                break;
        }

        for (Account acc : sortedUserAccounts) {
            System.out.print("Posts by ");
            System.out.println(acc);

            for (Post post : acc.getPosts()) {
                System.out.println(post);
            }
        }
    }
}
