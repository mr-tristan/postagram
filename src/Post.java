import java.util.ArrayList;
import java.time.LocalDate;

public class Post {
    private String content;
    private ArrayList<Account> likes;
    private LocalDate date;

    public Post(String content) {
        this.content = content;
        this.likes = new ArrayList<Account>();
        this.date = LocalDate.now();
    }

    public LocalDate getDate() {
        return this.date;
    }

    public int getLikes() {
        return this.likes.size();
    }

    public boolean addLike(Account acc) {
        for (Account a : likes) {
            if (a.getUsername().equalsIgnoreCase(acc.getUsername())) {
                return false;
            }
        }

        likes.add(acc);
        return true;
    }

    public String toString() {
        return "============================================\nPosted on " + this.date + "\n\n" + this.content + "\n\n" + this.likes.size() + " likes\n============================================\n" ;
    }
}
