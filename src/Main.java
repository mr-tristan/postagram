import java.util.Scanner;

public class Main {
    private static Postagram postagram = new Postagram();

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String cmd = "";

        while (!cmd.equals("exit")) {
            System.out.print("> ");
            cmd = in.nextLine();

            if (cmd.equals("signin")) {
                if (postagram.getCurrentUser() != null) {
                    System.out.println("Looks like " + postagram.getCurrentUser() + " is already signed in! Do you want to log them off and sign in as a different user?");
                    System.out.println("Y/N");
                    String choice = in.nextLine();

                    if (choice.equalsIgnoreCase("n")) {
                        continue;
                    }
                }

                System.out.print("Username: ");
                String username = in.nextLine();
                System.out.print("Password: ");
                String password = in.nextLine();
                Account acc = new Account(username, password);

                if (postagram.signIn(acc)) {
                    System.out.println("Successfully signed in as " + postagram.getCurrentUser());
                } else {
                    System.out.println("Whoops! Looks like those login credentials aren't correct.");
                }
            } else if (cmd.equals("whoami")) {
                if (postagram.getCurrentUser() != null) {
                    System.out.println(postagram.getCurrentUser());
                }
            } else if (cmd.equals("post")) {
                if (postagram.isCurrentUserSignedIn())  {
                    System.out.print("Message: ");
                    String message = in.nextLine();
                    Post post = new Post(message);

                    if (postagram.post(postagram.getCurrentUser(), post)) {
                        System.out.println("Success! Type `browse` to view all posts on Postagram.");
                    } else {
                        System.out.println("Whoops! We had some trouble posting this for you. Sign in and try again.");
                    }
                } else {
                    System.out.println("Please sign in first!");
                }
            } else if (cmd.equals("browse")) {
                Postagram.TimelineSort sortMethod = Postagram.TimelineSort.NONE;
                System.out.println("Would you like to sort your timeline? `likes` or `date`");
                String selectedSortMethod = in.nextLine();

                if (selectedSortMethod.equalsIgnoreCase("likes")) {
                    sortMethod = Postagram.TimelineSort.LIKES;
                } else if (selectedSortMethod.equalsIgnoreCase("date")) {
                    sortMethod = Postagram.TimelineSort.DATE;
                }

                postagram.displayTimeline(sortMethod);
            } else if (cmd.equals("register")) {
                System.out.print("Username: ");
                String username = in.nextLine();
                System.out.print("Password: ");
                String password = in.nextLine();

                if (postagram.registerAccount(username, password)) {
                    System.out.println("Successfully registered " + username + "! Welcome to Postagram!");
                } else {
                    System.out.println("Whoops! That name is already taken by another user.");
                }
            } else if (cmd.equals("like")) {
                System.out.print("Username: ");
                String username = in.nextLine();
                System.out.print("Post Number: ");
                int postNumber = Integer.parseInt(in.nextLine());

                if (postagram.likePost(username, postagram.getCurrentUser(), postNumber)) {
                    System.out.println("Success! You've liked " + username + "'s post.");
                } else {
                    System.out.println("Whoops! We had some trouble processing your request. Please try again.");
                }
            }
        }
    }
}
